/**
* @snippet
<div loading>
<img src="someimage.jpg">
</div>
*/


var $ = require('jquery');
var imagesLoaded = require('imagesloaded');

// constructor
var PageLoad = function(element, properties) {
    var self      = this;

    self.element  = element;
    self.$element = $(element);

    self.props    = $.extend({}, PageLoad.DEFAULTS, properties);

    self.assets = {
        loader: new imagesLoaded(self.element, {background: true}, function(){}),
        get progress() {
            return self.assets.loader.progressedCount;
        },
        get total_images() {
            return self.assets.loader.images.length;
        },
        loaded: false
    };

    self.cache = self.$element.clone();
    self._bindEvents();
    self._init();
};

PageLoad.SELECTOR = '[loading]';
PageLoad.DEFAULTS = {
    property: 'opacity',
    start: 0,
    end: 1
};
PageLoad.INSTANCES = [];


PageLoad.prototype._init = function() {
    var self = this;
    // do initialization stuff here.
    self.assets.loader.on('always', function(instance) {
        self.$element.trigger('body.pageload.zion');
    });
    self.assets.loader.on('progress', function(instance, image) {
        self.$element.trigger('image.pageload.zion');
    });
    self.$element.trigger('init.pageload.zion');
};


PageLoad.prototype._bindEvents = function() {
    var self  = this;
    // when stuff happens, do this other stuff.
    self.$element.on('init.pageload.zion', function() {
        self.setProperty(self.assets.start);
    });
    self.$element.on('image.pageload.zion', function() {
        self.setProperty(self.assets.images);
    });
    self.$element.on('body.pageload.zion', function() {
        self.finish();
    });
};

PageLoad.prototype.setProperty = function() {
    var self = this;
    var property = self.props.end / (self.assets.total_images / self.assets.progress);
    // do prepwork here...
    self.$element.trigger('beforesetprop.pageload.zion');
    self.$element.css(self.props.property, property);
    self.$element.trigger('aftersetprop.pageload.zion');
};

PageLoad.prototype.finish = function() {
    var self = this;
    self.$element.trigger('beforefinalprop.pageload.zion');
    self.assets.loaded = true;
    self.$element.attr('loading', false);
    self.$element.css(self.props.property, '');
    self.$element.trigger('afterfinalprop.pageload.zion');
};


PageLoad.prototype.destroy = function() {
    var self = this;
    self.$element.trigger('beforedestroy.pageload.zion');
    self.$element.replaceWith(self.cache);
};

////////////////////////////////////////////////////////////////////////////////
// Instatiate
$(document).ready(function() {
    function Plugin(option) {
        return this.each(function () {
            var $this       = $(this);
            var data        = $this.data('zion.pageload');
            var options     = $.extend({}, PageLoad.DEFAULTS, $this.data(), typeof option == 'object' && option);
            var pageload   = new PageLoad(this, options);
            PageLoad.INSTANCES.push(pageload);
            if (!data) {
                $this.data('zion.pageload', (data = pageload));
            }
            if (typeof option == 'string') data[option]();
        });
    }
    var old                    = $.fn.pageload;
    $.fn.pageload             = Plugin;
    $.fn.pageload.Constructor = PageLoad;
    $.fn.pageload.noConflict  = function () {
        $.fn.pageload = old;
        return this;
    };
    $(PageLoad.SELECTOR).each(function() {
        var $this   = $(this);
        var data    = $this.data('zion.pageload');
        var option  = data && $this.data();
        Plugin.call($this, option);
    });
});


module.exports = PageLoad;
